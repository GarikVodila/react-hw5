import styles from "./ProductList.module.scss";
import ProductCard from "../productCard";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
export default function ProductList(props) {
  let { products } = useSelector((state) => state.products);
  const { inCart: cart } = useSelector((state) => state.cart);
  const { inFavorites: favorites } = useSelector((state) => state.favorites);

  if (props.filter === "cart") {
    products = products.filter((product) => cart.includes(product.article));
  }
  if (props.filter === "favorites") {
    products = products.filter((product) =>
      favorites.includes(product.article)
    );
  }

  return (
    <ul className={styles.productList}>
      {products.map((product) => (
        <ProductCard
          key={product.article}
          {...product}
          isFavorite={favorites.includes(product.article)}
          inCart={cart.includes(product.article)}
        />
      ))}
    </ul>
  );
}

ProductList.propTypes = {
  filter: PropTypes.string,
};

ProductList.defaultProps = {
  filter: "",
};
