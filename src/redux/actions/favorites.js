import { favoritesTypes } from "../types";

export function addToFavorites(id) {
  return function (dispatch) {
    dispatch({
      type: favoritesTypes.ADD_TO_FAVORITES,
      payload: { id },
    });
  };
}

export function deleteFromFavorites(id) {
  return function (dispatch) {
    dispatch({
      type: favoritesTypes.DELETE_FROM_FAVORITES,
      payload: { id },
    });
  };
}
