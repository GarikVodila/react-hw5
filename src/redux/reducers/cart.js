import { cartTypes } from "../types";
import {
  getItemFromLocalStorage,
  removeItemFromLocalStorage,
  setItemToLocalStorage,
} from "../../utility/localStorage";

const initialState = {
  inCart: getItemFromLocalStorage("productInCart"),
};

export function cartReducer(state = initialState, action) {
  switch (action.type) {
    case cartTypes.ADD_TO_CART: {
      if (!state.inCart.includes(action.payload.id)) {
        const newCart = [...state.inCart, action.payload.id];
        setItemToLocalStorage("productInCart", newCart);
        return {
          inCart: [...newCart],
        };
      }
      break;
    }
    case cartTypes.DELETE_FROM_CART:
      if (state.inCart.includes(action.payload.id)) {
        const newCart = state.inCart.filter(
          (product) => product !== action.payload.id
        );
        setItemToLocalStorage("productInCart", newCart);
        return {
          inCart: [...newCart],
        };
      }
      break;
    case cartTypes.CLEAR_CART:
      removeItemFromLocalStorage(action.payload.target);
      return {
        inCart: [],
      };
    default:
      return state;
  }
}
