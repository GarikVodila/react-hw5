import ProductList from "../components/productList";
import NoProducts from "../components/noProducts";
import { useSelector } from "react-redux";
import BuyProductForm from "../components/buyProductForm";
import styles from "./styles/Cart.module.scss";

export function Cart() {
  const { inCart: cart } = useSelector((state) => state.cart);

  return (
    <>
      <main className="main">
        <section className="container">
          {cart.length === 0 ? (
            <NoProducts target="cart" />
          ) : (
            <div className={styles.wrap}>
              <ProductList filter="cart" />
              <BuyProductForm />
            </div>
          )}
        </section>
      </main>
    </>
  );
}
