import { modalTypes } from "../types";

const initialState = {
  modalIsOpen: false,
  modalData: {
    header: "",
    text: "",
    actions: [],
    closeButton: true,
  },
};

export function modalReducer(state = initialState, action) {
  switch (action.type) {
    case modalTypes.OPEN_MODAL:
      return {
        ...state,
        modalIsOpen: !state.modalIsOpen,
        modalData: {
          ...state.modalData,
          header: action.payload.header,
          text: action.payload.text,
          actions: action.payload.actions,
          closeButton: action.payload.closeButton,
        },
      };
    case modalTypes.CLOSE_MODAL:
      return {
        ...state,
        modalIsOpen: !state.modalIsOpen,
        modalData: {
          ...state.modalData,
          header: "",
          text: "",
          actions: [],
          closeButton: true,
        },
      };
    default:
      return state;
  }
}
