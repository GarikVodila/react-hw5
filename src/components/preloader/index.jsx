import styles from "./Preloader.module.scss";

export default function Preloader() {
  return (
    <div className={styles.loaderWrap}>
      <span className={styles.loader}></span>
    </div>
  );
}
