import PropTypes from "prop-types";

export default function StarIcon(props) {
  return (
    <svg
      className={props.className}
      onClick={() => props.onClick(props.article)}
      width="117"
      height="112"
      viewBox="0 0 117 112"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      {props.fill ? (
        <path
          d="M58.5 1.61804L71.8321 42.65L71.9443 42.9955H72.3076H115.451L80.5473 68.3546L80.2534 68.5681L80.3657 68.9136L93.6977 109.946L58.7939 84.5864L58.5 84.3729L58.2061 84.5864L23.3023 109.946L36.6343 68.9136L36.7466 68.5681L36.4527 68.3546L1.54886 42.9955H44.6924H45.0557L45.1679 42.65L58.5 1.61804Z"
          fill={props.color}
          stroke={props.color}
        />
      ) : (
        <path
          d="M58.5 3.23607L71.3566 42.8045L71.5811 43.4955H72.3076H113.912L80.2534 67.9501L79.6656 68.3771L79.8901 69.0681L92.7467 108.637L59.0878 84.1819L58.5 83.7548L57.9122 84.1819L24.2533 108.637L37.1099 69.0681L37.3344 68.3771L36.7466 67.9501L3.08771 43.4955H44.6924H45.4189L45.6434 42.8045L58.5 3.23607Z"
          stroke={props.color}
          strokeWidth="2"
        />
      )}
    </svg>
  );
}

StarIcon.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  article: PropTypes.number,
  fill: PropTypes.bool,
  color: PropTypes.string,
};

StarIcon.defaultProps = {
  onClick: function () {},
  color: "#0A84FF",
};
